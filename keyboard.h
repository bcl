#ifndef _keyboard_h
#define _keyboard_h

extern DECLSPEC int isKeyDown(int);
extern DECLSPEC int isKeyPressed(int key);
extern DECLSPEC int isKeyReleased(int key);
extern DECLSPEC int getReleasedKey();
extern DECLSPEC int getPressedKey();
extern DECLSPEC char getPressedChar();


#endif
