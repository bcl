%module bcl
%{
#include <bcl/bcl.h>
%}
struct Mouse {
int button1;
int button2;
int button3;
int button4;
int button5;
int x;
int y;
};
extern void initEnvironment();
extern void terminateEnvironment();
extern void pumpEvents();
extern void delay(int ms);
extern bool isKeyDown(int key);
extern bool isKeyPressed(int key);
extern bool isKeyReleased(int key);
extern int getPressedKey();
extern int getReleasedKey();
extern int getPressedChar();
extern struct Mouse *getMouseState(void);
extern void setMousePosition(int x, int y);
extern int isMouseButtonDown(int b);
extern void initSpeech();
extern void terminateSpeech();
extern void speak(char * text, int flags);
extern void setSpeechRate(int rate);
extern int getSpeechRate();
extern int isSpeaking();
extern void stopSpeech();
extern void initAudio();
extern void terminateAudio();
extern int loadSound(char * file);
extern int streamSound(char * file);
extern void unloadSound(int sound);
extern void setSoundPosition(int sound, float x, float y, float z);
extern void setListenerPosition(float x, float y, float z);
extern void playSound(int sound);
extern void loopSound(int sound);
extern void stopSound(int sound);
extern bool isSoundPlaying(int sid);
extern void setSoundLowpass(int sound, bool lp);
extern bool getSoundLowpass(int sound);
extern void setSoundPitch(int sound, float pitch);
extern void setSoundGain(int sound, float gain);
extern void setListenerOrientation(float x1, float y1, float z1, float x2, float y2, float z2);
