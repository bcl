#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include "mouse.h"
mouse *getMouseState(void)
{
mouse *m;
int k;
Uint8 buttons;
m =calloc(1, sizeof(mouse));
buttons  =SDL_GetMouseState(&m -> x, &m ->y);
for (k=0;k <5;k++){
if (SDL_BUTTON(k+1)&buttons)
*(((int *) m) +k) = 1;
}
return m;
}

void setMousePosition(int x, int y)
{
SDL_WarpMouse(x, y);
//todo check if propper cast to Uint16 is required
}

int isMouseButtonDown(int b)
{
if (b <1 ||b >5) return 0;
mouse *m =getMouseState();
int state =*(((int *) m) -1 +(sizeof(int)*b));
return state ==1;
}
