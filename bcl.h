#ifndef _bcl_h
#define _bcl_h

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __WIN32__
#ifdef BUILDBCL
#   define DECLSPEC     __declspec(dllexport)
#else
#    define DECLSPEC    __declspec(dllimport)
#endif
#else
#define DECLSPEC
#endif

#ifndef bool
typedef enum {
false = 0,
true} bool;
#endif

#include "environment.h"
#include "keyboard.h"
#include "mouse.h"
#include "timing.h"
#include "audio.h"
#include "speech.h"

#ifdef __cplusplus
}
#endif
#endif
