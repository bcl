#ifndef _mouse_h
#define _keyboard_h

typedef struct mouse_t {
int button1;
int button2;
int button3;
int button4;
int button5;
int x;
int y;
} mouse;
extern DECLSPEC mouse *getMouseState(void);
extern DECLSPEC void setMousePosition(int x, int y);
extern DECLSPEC int isMouseButtonDown(int b);

#endif
