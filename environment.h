#ifndef _window_h
#define _window_h

extern DECLSPEC void initEnvironment();
extern DECLSPEC void terminateEnvironment();
extern DECLSPEC void pumpEvents();

#endif
