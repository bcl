#ifndef _timing_h
#define _timing_h

#include <SDL/SDL_timer.h>

typedef SDL_TimerID TIMER;
typedef SDL_NewTimerCallback TIMERCALL;

extern DECLSPEC void delay(int);

extern DECLSPEC TIMER addTimer(int ms, TIMERCALL callback, void * data);
extern DECLSPEC void cancelTimer(TIMER timer);

#endif
