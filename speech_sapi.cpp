   #include <vole/vole.hpp>

   #include <comstl/util/initialisers.hpp>

   #include <iostream>
#include "bcl.h"
using namespace stlsoft;
using namespace std;

string voicestr;

vole::object getSpeaker() {
static vole::object ret =vole::object::create("SAPI.SpVoice");
cout<<sizeof(vole::object);
return ret;
}
vole::collection getVoices() {
static vole::collection ret =getSpeaker().invoke_method<vole::collection>(L"GetVoices");
return ret;
}
DECLSPEC void initSpeech() {
       try
       {
           comstl::com_initialiser coinit;
vole::object voice =getSpeaker().get_property<vole::object>(L"voice");
voicestr =voice.invoke_method<string>(L"GetDescription");
cout<<"hello";
}
       catch(std::bad_alloc&)
       {
           std::cerr << "out of memory" << std::endl;
       }
       catch(vole::vole_exception& x)
       {
           std::cerr << "operation failed: " << x.what() << ": " << winstl::error_desc_a(x.hr()) << std::endl;
       }
       catch(std::exception& x)
       {
           std::cerr << "operation failed: " << x.what() << std::endl;
       }
       catch(...)
       {
           std::cerr << "unexpected condition" << std::endl;
       }
}

DECLSPEC void terminateSpeech() {
return;
}
DECLSPEC void speak(char * text, int flags)
{
int sapiFlags =1;
if (flags &SPEAK_BLOCKING) sapiFlags =0;
if (flags &SPEAK_INTERRUPTING) sapiFlags =2;
cout <<"I was he2re";
getSpeaker().invoke_method<void>(L"Speak", "<voice required=\"name="+voicestr+"\">"+text+"</voice>");
}

DECLSPEC void setSpeechRate(int rate)
{
getSpeaker().put_property<int>(L"Rate",rate);
}

DECLSPEC int getSpeechRate() {
int ret = getSpeaker().get_property<int>(L"rate");
return ret;
}

DECLSPEC void setSpeechVoice(int v)
{
    int count = getVoices().get_property<int>(L"count");
if (v >=count) return;
vole::object voice = getVoices().invoke_method<vole::object>(L"Item",v);
voicestr =voice.invoke_method<string>(L"GetDescription");
}

DECLSPEC int getSpeechVoice() 
{
    int count = getVoices().get_property<int>(L"count");
for (int i =0;i<count;i++) 
{
vole::object voice =getVoices().invoke_method<vole::object>(L"item", i);
if (voicestr ==voice.invoke_method<string>(L"GetDescription")) return i;
}
return 0;
}

DECLSPEC void stopSpeech() 
{
getSpeaker().invoke_method<void>(L"Speak", "", 2);
}

