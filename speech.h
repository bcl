#ifndef _speech_h
#define _speech_h

#define SPEAK_BLOCKING 1
#define SPEAK_INTERRUPTING 2

extern DECLSPEC void initSpeech();
extern DECLSPEC void terminateSpeech();
extern DECLSPEC void speak(char * text,int flags);
extern DECLSPEC void setSpeechRate(int rate);
extern DECLSPEC int getSpeechRate();
extern DECLSPEC void setSpeechVoice(int voice);
extern DECLSPEC int getSpeechVoice();
extern DECLSPEC void stopSpeech();

// todo: add more prototypes

#endif
