#include "Sound.hpp"

Sound::Sound(char * file) {
sid = loadSound(file);
}

Sound::Sound(char * file, bool streaming) {
if (streaming)
sid = streamSound(file);
else
sid=loadSound(file);
}

Sound::~Sound() {
unloadSound(sid);
}

void Sound::play() {
playSound(sid);
}

void Sound::loop() {
loopSound(sid);
}

void Sound::stop() {
stopSound(sid);
}

void Sound::setPosition(float x, float y, float z) {
setSoundPosition(sid,x,y,z);
}

void Sound::setPitch(float p) {
setSoundPitch(sid,p);
}

void Sound::setLowpass(bool l) {
setSoundLowpass(sid,l);
}

bool Sound::getLowpass() {
return getSoundLowpass(sid);
}
