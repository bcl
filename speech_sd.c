#include "bcl.h"
#include <libspeechd.h>
#include <stdio.h>

SPDConnection * spd;
int myrate = 0;
int myvoice;
char *voices[8] = {"male1","male2","male3","female1","female2","female3","child_male","child_female"};
int speaking=0;

void mycallback(size_t a, size_t b, SPDNotificationType state) {
speaking=0;
}

void initSpeech() {
spd = spd_open("bcllib",NULL,NULL,SPD_MODE_THREADED);
spd->callback_end=spd->callback_cancel=mycallback;
spd_set_notification_on(spd,SPD_END);
myrate = 50;
myvoice=0;
spd_set_voice_rate(spd,myrate);
spd_set_synthesis_voice(spd,voices[myvoice]);
}

void terminateSpeech() {
spd_close(spd);
}

void speak(char * text, int flags) {
speaking=1;
if (flags&SPEAK_INTERRUPTING)
spd_cancel(spd);
spd_say(spd,SPD_MESSAGE,text);
if (flags&SPEAK_BLOCKING)
while (isSpeaking()) {
pumpEvents();
delay(20);
}
}

int getSpeechRate() {
return myrate;
}

void setSpeechRate(int rate) {
myrate = rate;
spd_set_voice_rate(spd,myrate);
}

int getSpeechVoice() {
return myvoice;
}

void setSpeechVoice(int voice) {
myvoice = voice;
spd_set_synthesis_voice(spd,voices[myvoice]);
}

int isSpeaking() {
return speaking;
}

void stopSpeech() {
spd_cancel(spd);
}
