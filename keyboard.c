#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

Uint8 *oldkeys=NULL;

DECLSPEC int isKeyDown(int key) {
int num = 0;
Uint8 *keystate = SDL_GetKeyState(&num);
return keystate[key]? 1:0;
}

DECLSPEC int isKeyPressed(int key) {
int num;
int ret=0;
Uint8 *keystate = SDL_GetKeyState(&num);
if (oldkeys==NULL)
oldkeys=calloc(num,1);
if (keystate[key]&&!oldkeys[key])
ret=1;
oldkeys[key] = keystate[key];
return ret;
}

DECLSPEC int isKeyReleased(int key) {
int num;
int ret=0;
Uint8 *keystate = SDL_GetKeyState(&num);
if (oldkeys==NULL)
oldkeys=calloc(num,1);
if (!keystate[key]&&oldkeys[key])
ret=1;
oldkeys[key] = keystate[key];
return ret;
}

DECLSPEC int getPressedKey() {
int i;
for (i = 1; i < SDLK_LAST; i++)
if (isKeyPressed(i))
return i;
return 0;
}

DECLSPEC int getReleasedKey() {
int i;
for (i = 1; i < SDLK_LAST; i++)
if (isKeyReleased(i))
return i;
return 0;
}

DECLSPEC char getPressedChar() {
// nead to use the window queue for this
char ret;
SDL_Event evt;
if (!SDL_PollEvent(&evt))
return 0;
if (evt.type!=SDL_KEYDOWN)
return 0;
ret = evt.key.keysym.unicode&0x7F;
return ret;
}
