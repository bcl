#include "bcl.h"

class Sound {
public:
Sound(char * file);
Sound(char * file, bool streaming);
~Sound();
void setPosition(float x, float y, float z);
void setPitch(float p);
void play();
void stop();
void loop();
void setLowpass(bool l);
bool getLowpass();
private:
int sid;
};
