#include <SDL/SDL.h>

DECLSPEC void initEnvironment() {
SDL_Init(SDL_INIT_EVERYTHING);
SDL_SetVideoMode(800,600,0,0);
SDL_WM_GrabInput(SDL_GRAB_ON);
}

DECLSPEC void terminateEnvironment() {
SDL_VideoQuit();
SDL_Quit();
}

DECLSPEC void pumpEvents() {
SDL_PumpEvents();
}
