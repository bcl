#include "bcl.h"
#include "ScreenReaderApi.h"

void initSpeech() {

}

void terminateSpeech() {

}

void speak(char * text, int flags) {
if (flags&SPEAK_INTERRUPTING)
sayString(text,1);
else
sayString(text,0);
if (flags&SPEAK_BLOCKING)
while (isSpeaking()) {
pumpEvents();
delay(20);
}
}

int getSpeechRate() {
return 0;
}

void setSpeechRate(int rate) {
}

int getSpeechVoice() {
return 0;
}

void setSpeechVoice(int voice) {
}

int isSpeaking() {
return 0;
}

void cancelSpeech() {
stopSpeech();
}
