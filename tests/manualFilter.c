#include <bcl/bcl.h>
#include <AL/al.h>
#include <AL/efx.h>
#include <stdio.h>

main(int argc, char * * argv) {
initAudio();
int f = 0;
int s = loadSound(argv[1]);
alGenFilters(1,&f);
alFilteri(f,AL_FILTER_TYPE,AL_FILTER_LOWPASS);
alFilterf(f,AL_LOWPASS_GAIN,0.5f);
alSourcei(s,AL_DIRECT_FILTER,f);
playSound(s);
getchar();
terminateAudio();
return 0;
}
