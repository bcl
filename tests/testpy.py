import ctypes

bcl = ctypes.CDLL("/usr/lib/libbcl.so")

bcl.initEnvironment()
while bcl.isKeyDown(27) ==0:
  bcl.pumpEvents()
  bcl.delay(10)
bcl.terminateEnvironment()
