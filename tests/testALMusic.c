#include "../bcl.h"
#include <stdlib.h>

main(int argc, char * * argv) {
initEnvironment();
initAudio();
int sound = streamSound(argv[1]);
loopSound(sound);
while (!isKeyDown('q')) {
delay(10);
pumpEvents();
}
stopSound(sound);
unloadSound(sound);
terminateAudio();
terminateEnvironment();
exit(0);
return 0;
}
