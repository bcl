declare sub initEnvironment lib "bcl" alias "initEnvironment" ()
declare sub terminateEnvironment lib "bcl" alias "terminateEnvironment" ()
declare sub pumpEvents lib "bcl" alias "pumpEvents" ()
declare function isKeyDown lib "bcl" alias "isKeyDown" (ByVal key as Integer) as Integer
declare sub bclSleep lib "bcl" alias "sleep" (ByVal ms as Integer)

initEnvironment()
do
pumpEvents()
bclSleep(10)
loop until isKeyDown(27)
terminateEnvironment()
