#include "../bcl.h"

main(int argc, char * * argv) {
if (argc <= 1)
return 1;
initEnvironment();
initAudio();
loadMusic(argv[1]);
do {
pumpEvents();
delay(10);
if (isKeyPressed('p'))
playMusic();
else if (isKeyPressed('s'))
stopMusic();
else if (isKeyPressed('l'))
loopMusic();
if (isKeyPressed(' '))
pauseMusicToggle();
} while (! isKeyPressed('q'));
terminateAudio();
terminateEnvironment();
return 0;
}
