#include "../bcl.h"
#include <stdio.h>

main(int argc, char * * argv) {
initAudio();
int sound = loadSound(argv[1]);
printf("%d\n", sound);
float i = -50.0f;
loopSound(sound);
for (;i < 50.0f; i+=1.0f) {
setSoundPosition(sound,i,0.0f,0.0f);
delay(200);
}
getchar();
unloadSound(sound);
terminateAudio();
return 0;
}
