#include <bcl/bcl.h>
#include <stdio.h>

main(int argc, char * * argv) {
initAudio();
int s1 = loadSound(argv[1]);
int s2 = loadSound(argv[2]);
setSoundLowpass(s1,1);
// setSoundLowpass(s2,1);
setSoundPosition(s1,-1.0f,0.0f,0.0f);
setSoundPosition(s2,1.0f,0.0f,0.0f);
playSound(s1);
playSound(s2);
getchar();
terminateAudio();
return 0;
}
