#include "../bcl.h"
#include <stdio.h>
#include <stdlib.h>

main() {
int sounds[30];
init3dAudio();
int i;
for (i = 0; i < 30; i++) {
sounds[i] = load3dSound(i%2==0?"/home/rkruger/test.wav":"/home/rkruger/test2.wav");
set3dSoundPosition(sounds[i],4.0f/random(),3.0f/random(),6.0f/random());
set3dSoundPitch(sounds[i],0.5f+9.0f/random());
play3dSound(sounds[i]);
}
getchar();
terminate3dAudio();
return 0;
}
