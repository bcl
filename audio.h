#ifndef _audio_h
#define _audio_h

DECLSPEC void initAudio();
DECLSPEC void terminateAudio();
DECLSPEC int streamSound(char * file);
DECLSPEC int loadSound(char * file);
DECLSPEC void unloadSound(int sound);
DECLSPEC void setSoundPosition(int sound, float x, float y, float z);
DECLSPEC void playSound(int sound);
DECLSPEC void loopSound(int sound);
DECLSPEC void stopSound(int sound);
DECLSPEC void setSoundPitch(int sound, float pitch);
DECLSPEC void setListenerPosition(float x, float y, float z);
DECLSPEC void setSoundLowpass(int sound, int lp);
DECLSPEC int getSoundLowpass(int sound);
DECLSPEC int isSoundPlaying(int sound);
DECLSPEC void setSoundGain(int sound, float gain);
DECLSPEC void setListenerOrientation(float x1, float y1, float z1, float x2, float y2, float z2);
#endif
