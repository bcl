#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifndef _WIN32
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alure.h>
#include <AL/efx.h>
#else
#include <al.h>
#include <efx.h>
#include <alc.h>
#include <alure.h>
#endif
#include "bcl.h"

typedef struct {
char * fileName;
ALuint bufId;
int count;
alureStream * stream;
} clip;

clip * clips;
ALuint * sources;
int clipsSize=20;
int sourcesSize=20;
int numClips;
int numSources;
int lock;

int addClip(clip c) {
if (numClips>=clipsSize) {
clips = realloc(clips,clipsSize*2*sizeof(clip));
clipsSize*=2;
}
clips[numClips] = c;
numClips++;
return numClips-1;
}

void removeClip(int pos) {
for (;pos<numClips-1;pos++)
clips[pos]=clips[pos+1];
numClips--;
if (numClips>20&&numClips <clipsSize/4) {
clips = realloc(clips,clipsSize/2*sizeof(clip));
clipsSize/=2;
}
}

int findClipstr(char * name) {
int i = 0;
for (;i<numClips;i++)
if (strcmp(name,clips[i].fileName)==0)
return i;
return -1;
}

int findClipi(int id) {
int i = 0;
for (;i<numClips;i++)
if (clips[i].bufId==id)
return i;
return -1;
}

int addSource(ALuint id) {
if (numSources>=sourcesSize) {
sources = realloc(sources,sizeof(ALuint)*sourcesSize*2);
sourcesSize*=2;
}
sources[numSources]=id;
numSources++;
return numSources -1;
}

void removeSource(int pos) {
for (;pos<numSources-1;pos++)
sources[pos]=sources[pos+1];
numSources--;
if (numSources>20&&numSources<sourcesSize/4) {
sources = realloc(sources,sizeof(ALuint)*sourcesSize/2);
sourcesSize/=2;
}
}

int findSource(int id) {
int i;
for (i = 0; i < numSources;i++)
if (sources[i]==id)
return i;
return -1;
}

void sourcecall(void *data, ALuint source) {
}

int timercall(int ms, void * data) {
alureUpdate();
return ms;
}

int lowpass;
TIMER updater;

void waitLock() {
while (lock==1) {
delay(10);
}
lock=1;
}

DECLSPEC void initAudio() {
lock=0;
waitLock();
int attrlist[3];
attrlist[0] = ALC_FREQUENCY;
attrlist[1] = 44100;
attrlist[2] = 0;
alureInitDevice(NULL,attrlist);
numClips = 0;
numSources = 0;
clips = malloc(sizeof(clip)*clipsSize);
sources = malloc(sizeof(ALuint)*sourcesSize);
updater = addTimer(10,timercall,NULL);
// filters
alGenFilters(1,&lowpass);
alFilteri(lowpass,AL_FILTER_TYPE,AL_FILTER_LOWPASS);
alFilterf(lowpass,AL_LOWPASS_GAINHF,0.5f);
lock = 0;
}

DECLSPEC void terminateAudio() {
waitLock();
cancelTimer(updater);
int i;
alDeleteSources(numSources,sources);
for (i = 0; i < numClips;i++) {
free(clips[i].fileName);
if (clips[i].stream==NULL)
alDeleteBuffers(1,&clips[i].bufId);
else 
alureDestroyStream(clips[i].stream,4096,&clips[i].bufId);
}
free(clips);
free(sources);
alDeleteFilters(1,&lowpass);
alureShutdownDevice();
lock=0;
}

void playsource(int sound,int loop) {
int bufid;
alGetSourcei(sound, AL_BUFFER, &bufid);
int pos = findClipi(bufid);
if (pos==-1)
return;
if (clips[pos].stream==NULL)
alSourcePlay(sound);
else {
alurePlaySourceStream(sound,clips[pos].stream,4,loop,sourcecall,NULL);
}
}

DECLSPEC int loadSound(char * file) {
waitLock();
int pos = findClipstr(file);
ALuint bufid;
if (pos==-1) {
bufid = alureCreateBufferFromFile(file);
clip c= {strdup(file),bufid,0,NULL};
pos = addClip(c);
}
else
bufid = clips[pos].bufId;
ALuint soundid;
alGenSources(1, &soundid);
alSourcei(soundid, AL_BUFFER, bufid);
addSource(soundid);
clips[pos].count++;
lock=0;
return soundid;
}

DECLSPEC int streamSound(char * file) {
waitLock();
int pos = findClipstr(file);
ALuint bufid;
if (pos==-1) {
alureStream * s  = alureCreateStreamFromFile(file,4096,1,&bufid);
clip c= {strdup(file),bufid,0,s};
pos = addClip(c);
}
else
bufid = clips[pos].bufId;
ALuint soundid;
alGenSources(1, &soundid);
alSourcei(soundid, AL_BUFFER, bufid);
addSource(soundid);
clips[pos].count++;
lock=0;
return soundid;
}

DECLSPEC void unloadSound(int sound) {
waitLock();
if (!alIsSource(sound)) {
printf("bcl: Invalid source\n");
return;
}
int id,pos;
alGetSourcei(sound,AL_BUFFER,&id);
pos = findClipi(id);
removeSource(findSource(sound));
alDeleteSources(1, &sound);
clips[pos].count--;
if (clips[pos].count==0) {
free(clips[pos].fileName);
if (clips[pos].stream==NULL)
alDeleteBuffers(1,&clips[pos].bufId);
else
alureDestroyStream(clips[pos].stream,4096,&clips[pos].bufId);
removeClip(pos);
}
lock=0;
}

DECLSPEC void setSoundPosition(int sound, float x, float y, float z) {
alSource3f(sound,AL_POSITION,x,y,z);
}

DECLSPEC void setSoundPitch(int sound, float pitch) {
alSourcef(sound,AL_PITCH,pitch);
}

DECLSPEC void playSound(int sound) {
alSourcei(sound,AL_LOOPING,AL_FALSE);
playsource(sound,1);
}

DECLSPEC void loopSound(int sound) {
alSourcei(sound,AL_LOOPING,AL_TRUE);
playsource(sound,-1);
}

DECLSPEC void stopSound(int sound) {
alureStopSource(sound,0);
}


DECLSPEC void setListenerPosition(float x, float y, float z) {
alListener3f(AL_POSITION, x, y, z);
}

DECLSPEC void setSoundLowpass(int sound, int lp) {
if (lp)
alSourcei(sound,AL_DIRECT_FILTER,lowpass);
else
alSourcei(sound,AL_DIRECT_FILTER,AL_FILTER_NULL);
}

DECLSPEC int getSoundLowpass(int sound) {
int ret;
alGetSourcei(sound,AL_DIRECT_FILTER,&ret);
return ret==lowpass;
}

DECLSPEC int isSoundPlaying(int sound) {
if (!alIsSource(sound)) {
printf("bcl: Invalid source %d\n",sound);
return 0;
}
int ret=0;
alGetSourcei(sound,AL_SOURCE_STATE,&ret);
return ret==AL_PLAYING;
}

DECLSPEC void setSoundGain(int sound, float gain) {
alSourcef(sound,AL_GAIN,gain);
}


void setListenerOrientation(float x1, float y1, float z1, float x2, float y2, float z2) {
float attrs[6] = {x1,y1,z1,x2,y2,z2};
alListenerfv(AL_ORIENTATION,attrs);
}
