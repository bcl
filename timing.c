#include "timing.h"

DECLSPEC void delay(int ms) {
SDL_Delay(ms);
}

DECLSPEC TIMER addTimer(int ms, TIMERCALL callback, void * data) {
return SDL_AddTimer(ms,callback,data);
}

DECLSPEC void cancelTimer(TIMER timer) {
SDL_RemoveTimer(timer);
}
