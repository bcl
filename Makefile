all:
	gcc -ggdb -DBUILDBCL -o libbcl.so environment.c keyboard.c mouse.c timing.c audio.c  speech_sd.c -lSDL  -shared -lspeechd -lopenal -lalure

install:
	cp libbcl.so /usr/lib
	mkdir -p /usr/include/bcl
	cp *.h /usr/include/bcl

clean:
	rm libbcl.so
